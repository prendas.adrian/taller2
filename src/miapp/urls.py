from django.contrib import admin
from django.urls import path
from django.conf.urls import url


from miapp import views

urlpatterns = [
    path("csv", views.descargue_csv, name="download"),
    path("", views.miindex, name="pag_principal"),
    path("hora", views.mi_vista_cacheada, name="vista_cacheada"),
    path("vista_autorizada", views.mi_vista_atorizada, name="vista_autorizada"),
    url("fecha/(?P<anio>\d+)/(?P<mes>enero|febrero|marzo)/(?P<dia>\d+)/", views.mis_fechas, name="fechas"),
]
#path("fecha/<int:anio>/<int:mes>/<int:dia>/", views.mis_fechas, name="fechas")